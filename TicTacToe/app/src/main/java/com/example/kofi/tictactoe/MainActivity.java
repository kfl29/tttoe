package com.example.kofi.tictactoe;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kofi.tictactoe.GameBoard;


public class MainActivity extends AppCompatActivity
{
    private GameBoard board = null;
    private int x, y, count = 0;
    private String player1 = "X";
    private String player2 = "O";
    private Boolean endGame = null;
    String currentP = player1;

    //Create the initial game space
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        board = new GameBoard();
    }

    public void squareClick(View v)
    {
//Get the id of the clicked object and assign it to a Textview variable
        TextView square = (TextView) findViewById(v.getId());
//Check the content and make sure the square is empty and that the game isn't over
        String content = (String) square.getText();
        if (content == "" && !endGame)
        {

//Find the X Y location values of the particular square that was clicked
            switch (square.getId())
            {

                case R.id.square11:
                    x = 0;
                    y = 0;
                    break;
                case R.id.square12:
                    x = 0;
                    y = 1;
                    break;
                case R.id.square13:
                    x = 0;
                    y = 2;
                    break;
                case R.id.square21:
                    x = 1;
                    y = 0;
                    break;
                case R.id.square22:
                    x = 1;
                    y = 1;
                    break;
                case R.id.square23:
                    x = 1;
                    y = 2;
                    break;
                case R.id.square31:
                    x = 2;
                    y = 0;
                    break;
                case R.id.square32:
                    x = 2;
                    y = 1;
                    break;
                case R.id.square33:
                    x = 2;
                    y = 2;
                    break;

            }
            //Place the player's mark on the specific X Y location on both the virtual and displayed board
            board.setSquare(x, y,currentP);
            square.setText(currentP);
            //Increment move Count because a move was just made
            count++;

//Check to see if the game is over
            endGame = checkEnd(currentP);

            if(currentP == player1)
            {
                currentP = player2;
            }
            else
            {
                currentP = player1;
            }
        }
    }

    private boolean checkEnd(String player) {
//Checks the virtual board for a winner if there's a winner announce it with the provided player
        if (board.checkWin(player))
        {

            announce(true, player);
            return true;
        }

//Check to see if we've reached our move total meaning it's a draw
        else if (count >= 9)
        {

            announce(false, player);
            return true;

        }
//If neither win or draw then the game is still on
        return false;

    }

    private void reset() {
//Get the id list of all the Textview squares
        int[] idList = { R.id.square11, R.id.square12, R.id.square13, R.id.square21, R.id.square22, R.id.square23, R.id.square31, R.id.square32, R.id.square33 };
        TextView square;
//For each square clear the text with an empty string
        for (int item : idList) {

            square = (TextView) findViewById(item);
            square.setText("");

        }

//Reset the game state and clear the virtual board
        endGame = false;
        count = 0;
        board.resetBoard();

    }


    //Announce the winner, given a boolean for whether it was a win or a draw
// and given the last player to make a mark
    private void announce(boolean endState, String player)
    {
//Check for if it's a win or a draw. if it's a win amend player with wins!
// if it's a lose replace player with it's a draw! I did this just because why
// declare another String when I can just reuse the one I have?
        if (endState == true)

            player = player + " wins!";

        else

            player = "It's a draw!";

//Get the application Context and setup the Toast notification with the end state info
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, player, Toast.LENGTH_LONG);
        toast.show();

    }

    public void resetClick(View v)
    {
        reset();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
