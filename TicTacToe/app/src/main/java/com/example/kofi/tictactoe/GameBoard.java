package com.example.kofi.tictactoe;

/**
 * Created by Kofi on 2/24/2016.
 */
public class GameBoard
{
    private String[][] board = new String[3][3];

    GameBoard()
    {
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
            {
                board[i][j] = " ";
            }
    }

    public void resetBoard()
    {
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
            {
                board[i][j] = " ";
            }
    }

    public String[][] getBoard()
    {
        return board;
    }

    public void setSquare(int x, int y, String p)
    {
        if(board[x][y] == " ")
            board[x][y] = p;
    }

    public Boolean checkRow(int i, String p)
    {
        for(int j = 0; j < 3; j++)
            if (board[i][j] != p)
                return false;
        return true;
    }

    public Boolean checkDiagonals(String p)
    {
        if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == p)
            return true;

        if (board[2][0] == board[1][1] && board[2][0] == board[0][2] && board[2][0] == p)
            return true;

        return false;
    }
    public Boolean checkWin(String p)
    {
        Boolean win;

        for(int i = 0; i < 3; i++)
        {
            win = checkRow(i, p);
            if(win = true)
                return win;
        }

        win = checkDiagonals(p);
        if(win = true)
            return win;

        return win;
    }


}
